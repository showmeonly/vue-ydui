import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const state = {
  isLoading: false
}

const mutations = {
  updateLoadingStatus(state, isLoading) {
    state.isLoading = isLoading
  }
}

const store = new Vuex.Store({
  state,
  mutations
})
export default store
