// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import App from './App'
import router from './router'
import fastclick from 'fastclick'
import axios from './http'
import 'babel-polyfill' // 解决某些浏览器不支持promise造成白屏问题
import store from './store'
import 'styles/reset.css'
import 'styles/border.css'
import 'styles/iconfont.css'
import 'vue-ydui/dist/ydui.rem.css'
import 'styles/mixins.styl'
import YDUI from 'vue-ydui' /* 相当于import YDUI from 'vue-ydui/ydui.rem.js' */
import Vconsole from 'vconsole'
Vue.use(YDUI)
/* eslint-disable */
const vconsole = new Vconsole()
/* eslint-enable */

Vue.config.productionTip = false
fastclick.attach(document.body) // 解决click时300毫秒延迟
Vue.prototype.bus = new Vue()
Vue.prototype.$http = axios

Vue.prototype.bus.$on('changeTitle', title => {
  const u = navigator.userAgent
  const isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1
  const isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)
  if (isAndroid) {
    document.title = title
  } else if (isiOS) {
    document.title = title
    const i = document.createElement('iframe')
    // i.src = 'src/assets/logo.png'; // 需要一个错误的地址
    i.style.display = 'none'
    i.onload = function () {
      setTimeout(() => {
        i.remove()
      }, 9)
    }
    document.body.appendChild(i)
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
