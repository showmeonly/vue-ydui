import axios from 'axios'
// 请求超时
axios.defaults.timeout = 6000
// 这里的config包含每次请求的内容
axios.interceptors.request.use(config => {
  config.headers['client-id'] = 'n56RVu9qqR84'
  config.headers['Content-Type'] = 'application/json'
  return config
}, function (err) {
  return Promise.reject(err)
})

// http interceptors
axios.interceptors.response.use(
  response => {
    if (response.data && response.data.errorCode) {
      alert(response.data.message)
    }
    // switch (response.data.errorCode) {
    //   case 400:
    //     // 未登录
    //     break
    //   case 500:
    //     alert('服务器异常')
    //     break
    //   case 1001:
    //     alert('服务器异常')
    // }
    return response
  },
  error => {
    const response = JSON.parse(JSON.stringify(error)).response
    const { data } = response
    const result = data || {}
    if ({}.hasOwnProperty.call(result, 'errorCode')) {
      alert(result.message)
      return Promise.reject(result)
      // switch (result.errorCode) {
      //   case 200:
      //     return result
      //   case 400:
      //     // 未登录
      //     break
      //   case 1005:
      //     alert('服务器异常')
      //     return Promise.reject(data)
      //   default:
      //     return Promise.reject(data)
      // }
    }
    Promise.reject(result)
  }
)

export default {
  // get请求
  get(url, param) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url,
        params: param
      }).then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  },
  // post请求
  post(url, param) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url,
        data: param
      }).then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
