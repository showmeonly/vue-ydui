import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Index from '@/pages/Index'
import Test from '@/pages/Test'
Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        title: '1111'
      }
    },
    {
      path: '/test',
      name: 'Test',
      component: Test,
      meta: {
        title: '2222'
      }
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

// 路由导航钩子，beforeEach，在路由进入前调用
router.beforeEach((to, from, next) => {
  Vue.prototype.bus.$emit('changeTitle', to.meta.title)
  /* 显示加载中动画 */
  store.commit('updateLoadingStatus', true)
  // 继续路由导航
  next()
})
router.afterEach(route => {
  /* 隐藏加载中动画 */
  store.commit('updateLoadingStatus', false)
})
export default router
